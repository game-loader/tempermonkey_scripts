// ==UserScript==
// @name         Auto Progress Learning with UI Notifications
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Automatically progress through courses, update UI, and handle course completion with notifications.
// @match        https://nmg.chinamde.cn/p_xuexi.html?id=*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let classhourList = [];
    let currentCourseIndex = 0;
    let saveInterval;

    // Function to get cookie by name
    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }

    // Extract useropenid from cookies
    const useropenid = getCookie('useropenid');

    // Extract id from URL
    const urlParams = new URLSearchParams(window.location.search);
    const courseId = urlParams.get('id');

    // Display progress window
    const progressDiv = document.createElement('div');
    progressDiv.style.position = 'fixed';
    progressDiv.style.left = '20px';
    progressDiv.style.top = '50%';
    progressDiv.style.transform = 'translateY(-50%)';
    progressDiv.style.backgroundColor = 'white';
    progressDiv.style.border = '1px solid black';
    progressDiv.style.padding = '10px';
    progressDiv.style.zIndex = '1000';
    document.body.appendChild(progressDiv);

    function updateProgressWindow(courseName, progress) {
        progressDiv.innerHTML = `当前课程: ${courseName}<br>进度: ${progress}`;
    }

    // Function to play the next video or notify if all videos have been played
    function playNextVideo(currentDataId) {
        var nextDataId = parseInt(currentDataId, 10);
        var nextVideoP = document.querySelector(`p[data-id="${nextDataId}"]`);

        // Check if there is a next video to play
        if (nextVideoP) {
            playVideo(nextDataId, 0);
            addNotification(`Playing video with data-id: ${nextDataId}`);
        } else {
            addNotification('学习完成');
        }
    }

    // Function to add notifications to the UI
    function addNotification(message) {
        // Implement this function based on how you want to show notifications
        console.log(message); // Placeholder for demonstration
    }

    // Function to send POST request to getChapterAndCoursehoursById
    function getChapterAndCoursehoursById() {
        const formData = new FormData();
        formData.append('id', courseId);
        formData.append('checked', '1');
        formData.append('Authentication', useropenid);
        formData.append('remoteType', 'pc');

        fetch('https://nmg.api.chinamde.cn/api.php?s=web/course/getChapterAndCoursehoursById', {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(data => {
            classhourList = data.data.classhourList;
            if (classhourList && classhourList.length > 0) {
                saveLearningRecord();
            }
        })
            .catch(error => {
            console.error('Error:', error);
        });
    }


    // Function to handle the next course learning
    function handleNextCourseLearning() {
        if (currentCourseIndex < classhourList.length) {
            const nextCourse = classhourList[currentCourseIndex];

            saveLearningRecord(); // Start POST requests for the next course
        } else {
            addNotification('All courses have been completed.');
        }
    }

    // Function to send POST request to saveLeaningRecord every 20 seconds
    function saveLearningRecord() {
        if (currentCourseIndex >= classhourList.length) {
            addNotification("All courses in classhourList have been completed.");
            return;
        }

        const currentCourse = classhourList[currentCourseIndex];

        // Check if course is already completed based on percent
        if (parseFloat(currentCourse.percent) >= 100) {
            console.log(`Course already completed: ${currentCourse.title}`);
            currentCourseIndex++;
            handleNextCourseLearning(); // Handle learning for the next course
            return;
        }
        playNextVideo(currentCourse.id); // Play the next video
        let second = Math.round(parseFloat(currentCourse.percent) / 100 * currentCourse.hourCount);
        const interval = 20; // Duration to wait between requests (20 seconds)


        function sendRequest() {
            if (second > currentCourse.hourCount) {
                console.log(`Completed course: ${currentCourse.title}`);
                currentCourseIndex++;
                if (currentCourseIndex < classhourList.length) {
                    const nextCourse = classhourList[currentCourseIndex];
                    playNextVideo(nextCourse.id); // Play the next video
                    saveLearningRecord(); // Start POST requests for the next course
                } else {
                    addNotification('All courses have been completed.');
                }
                return;
            }

            const formData = new FormData();
            formData.append('courseId', courseId);
            formData.append('hourId', currentCourse.id);
            formData.append('second', Math.min(second, currentCourse.hourCount));
            formData.append('duration', currentCourse.hourCount);
            formData.append('Authentication', useropenid);
            formData.append('remoteType', 'pc');

            fetch('https://nmg.api.chinamde.cn/api.php?s=web/course/saveLeaningRecord', {
                method: 'POST',
                body: formData
            })
                .then(response => response.json())
                .then(data => {
                if (data.errorMsg === "请求成功,课件已完结") {
                    console.log(`Course completed: ${currentCourse.title}`);
                    currentCourseIndex++;
                    if (currentCourseIndex < classhourList.length) {
                        const nextCourse = classhourList[currentCourseIndex];
                        playNextVideo(nextCourse.id); // Play the next video
                        saveLearningRecord(); // Start POST requests for the next course
                    } else {
                        addNotification('All courses have been completed.');
                    }
                } else {
                    console.log(`Saved learning record for course: ${currentCourse.title}, second: ${second}`);
                    updateProgressWindow(currentCourse.title, `${second}/${currentCourse.hourCount}`);
                    second += interval; // Increment by 20 seconds
                    setTimeout(sendRequest, interval * 1000); // Schedule the next request
                }
            })
                .catch(error => {
                console.error('Error:', error);
                setTimeout(sendRequest, interval * 1000); // Schedule the next request in case of error
            });
        }

        sendRequest(); // Start the first request
    }

    // Start the process immediately on page load for testing purposes
    getChapterAndCoursehoursById();
})();
